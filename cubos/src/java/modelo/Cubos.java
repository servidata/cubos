/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author fagp
 */
public class Cubos {
    public String calcularDiagonales(String entrada){
        String salida         ="";
        String operacion      ="";
        String flagCambioCubo = "S";
        Integer numIter       = 0;
        Integer sizeCubo      = 0;
        Integer numOper       = 0;
        Integer rangoIni      = 0;
        Integer rangoFin      = 0;
        Integer numDiagonal   = 0;
        Integer vlrSuma       = 0;
        Integer idxIter       = 0;
        Integer[] vlrDiagonales = new Integer[999];
        String[] lineas = entrada.split("\n");
        numIter = Integer.valueOf(lineas[0].trim());
        //salida = salida + " numIter : " + numIter + "\n";
        for (int i = 1; i < lineas.length; i++) {
            lineas[i] = lineas[i].trim();
            String[] parcero = lineas[i].split(" ");
            if (flagCambioCubo.equals("S")) {
                flagCambioCubo = "N";
                // Resetea la matriz al cambio de cubo
                for (int ii = 0; ii < 999; ii++) {
                    vlrDiagonales[ii] = new Integer(0);
                }
                for (int k = 0; k < parcero.length; k++) {
                    if (k == 0) {
                        sizeCubo = Integer.parseInt(parcero[k]);
                        //salida = salida + " sizeCubo : " + sizeCubo + "\n";
                    }
                    if (k == 1) {
                        numOper = Integer.parseInt(parcero[k]);
                        //salida = salida + " numOper : " + numOper + "\n";
                    }
                }
            } else {
                for (int k = 0; k < parcero.length; k++) {
                    if (k == 0) {
                        operacion = parcero[k];
                        //salida = salida + " operacion : " + operacion + "\n";
                        continue;
                    }
                    if (operacion.equals("UPDATE")) {
                        if (k == 1 || k == 2 || k == 3) {
                            numDiagonal = Integer.parseInt(parcero[k]);
                            //salida = salida + " numDiagonal : " + numDiagonal + "\n";
                            continue;
                        }
                        if (k == 4) {
                            vlrDiagonales[numDiagonal] = Integer.parseInt(parcero[k]);
                            //salida = salida + " zzz update vlrDiagonales[numDiagonal] : " + vlrDiagonales[numDiagonal] + "\n";
                        }
                    } else if (operacion.equals("QUERY")) {
                        if (k == 1 || k == 2 || k == 3) {
                            rangoIni = Integer.parseInt(parcero[k]);
                            //salida = salida + " rangoIni : " + rangoIni + "\n";
                            continue;
                        }
                        if (k == 4 || k == 5 || k == 6) {
                            rangoFin = Integer.parseInt(parcero[k]);
                            //salida = salida + " rangoFin : " + rangoFin + "\n";
                        }
                    }
                } 
                if (operacion.equals("QUERY")) {
                if (rangoFin > sizeCubo) {
                    rangoFin = sizeCubo;
                }
                for (int m = rangoIni; m <= rangoFin; m++) {
                    //salida = salida + " m : " + m + "\n";
                    vlrSuma = vlrSuma + vlrDiagonales[m];
                }
                //salida = salida + " rangoFin : " + rangoFin + "\n";
                salida = salida + "\n" + vlrSuma;
                vlrSuma=0;
                }
                idxIter++;
                if (idxIter == numOper) {
                    idxIter = 0;
                    flagCambioCubo = "S";
                }
            }
        }
    return salida;
    }
  
}
