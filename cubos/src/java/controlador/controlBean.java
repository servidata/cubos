/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import modelo.*;

/**
 *
 * @author aeg
 */
@ManagedBean
@RequestScoped
public class controlBean {

    String entrada;
    String salida = "";

    public void manejaBoton() {
        Cubos cubos = new Cubos();
        salida = cubos.calcularDiagonales(entrada);
    }

    public String getEntrada() {
        return entrada;
    }

    public void setEntrada(String entrada) {
        this.entrada = entrada;
    }

    public String getSalida() {
        return salida;
    }

    public void setSalida(String salida) {
        this.salida = salida;
    }

}

